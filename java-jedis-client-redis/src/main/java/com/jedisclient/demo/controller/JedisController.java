package com.jedisclient.demo.controller;

import com.jedisclient.demo.dao.JedisUtil;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@AllArgsConstructor
public class JedisController {

    @Autowired
    private JedisUtil jedisUtil;

    // http://localhost:8087/wiener/user/testRedisSave?id=1090330
    @RequestMapping("/testRedisSave")
    public Object testRedisSave(Long id) {
        jedisUtil.del("userId:"+id);
        jedisUtil.set(id.toString(), "The test address is " + UUID.randomUUID().toString());
        return null;
    }
    // http://localhost:8087/wiener/user/testRedisGet?id=1090330
    @RequestMapping("/testRedisGet")
    public String testRedisGet(Long id) {
        String myStr= jedisUtil.get("userId:"+ id);
        if(!StringUtils.isEmpty(myStr)) {
            return myStr;
        }
        return null;
    }
}