Conectando usando redis-cli  
redis-cli es una sencilla herramienta de línea de comandos para interactuar con la base de datos de Redis.

Utilice "docker exec" para cambiar su contexto al contenedor de software empresarial de Redis

docker exec -it rp bash

Ejecute redis-cli, ubicado en el directorio / opt / redislabs / bin, para conectarse 
al número de puerto de la base de datos y almacenar y recuperar una clave en database1.

$ /opt/redislabs/bin/redis-cli -p 12000
127.0.0.1:16653> set key1 123
OK
127.0.0.1:16653> get key1
"123"