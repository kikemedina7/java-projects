package com.spring.aop.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@ToString
public class Customer {

    private String name;
    private String lastName;
    private Integer  age;
    private String email;
    
}
