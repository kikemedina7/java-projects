package com.spring.aop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSpringAopTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSpringAopTestApplication.class, args);
	}

}
