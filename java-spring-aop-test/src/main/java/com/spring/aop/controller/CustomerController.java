package com.spring.aop.controller;

import com.spring.aop.dto.Customer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {


    @GetMapping(value = "/listCustomer",produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity getCustomer(){
        Customer customer = Customer.builder()
                                .name("Jorge")
                                .lastName("Medina")
                                .age(26)
                                .email("systemsjemb@gmail.com")
                                .build();
        return new ResponseEntity(customer, HttpStatus.OK);
    }

}
