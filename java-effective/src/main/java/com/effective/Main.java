package com.effective;

public class Main {
    private static final System.Logger LOGGER = System.getLogger("");

    public static void main(String[] args) {
        LOGGER.log(System.Logger.Level.INFO, "Hello, {}!", "user");
    }

}
