package com.effective.creating_destroying_objects.item5;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Main {

    /**
     * Prefer dependency injection to hardwiring
     * resources
     *
     * @param args
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.init();
    }

    public void init() {
        SpellChecker spellChecker = new SpellChecker<String>("este es el universal");
        spellChecker.suggestions("2");
    }

}

// Inappropriate use of static utility - inflexible & untestable!
class SpellCheckerNone1 {
    private static final String dictionary = "";

    private SpellCheckerNone1() {
    } // Noninstantiable

    public static boolean isValid(String word) {
        return Boolean.TRUE;
    }

    public static List<String> suggestions(String typo) {
        return Collections.emptyList();
    }
}

// Inappropriate use of singleton - inflexible & untestable!
class SpellCheckerNone2 {
    private final String dictionary = "";

/*    private SpellCheckerNone2(){}

    public static INSTANCE = new SpellCheckerNone2();*/

    public boolean isValid(String word) {
        return Boolean.TRUE;
    }

    public List<String> suggestions(String typo) {
        return Collections.emptyList();
    }
}

// Dependency injection provides flexibility and testability

 class SpellChecker<Lexicon> {
    private final Lexicon dictionary;

    public SpellChecker(Lexicon dictionary) {
        this.dictionary =
                Objects.requireNonNull(dictionary);
    }

    public boolean isValid(String word) { return Boolean.TRUE;}

    public List<String> suggestions(String typo) { return Collections.emptyList();}
};
