package com.effective.creating_destroying_objects.item2.example1;

public class Main {

    /**
     *  Consider a builder when faced with many
     * constructor parameters
     * @param args
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.init();
    }

    public void init(){
        //No debe hacerse
        NutritionFactsManyConstructors cocaCola =
                new NutritionFactsManyConstructors(240, 8, 100, 0, 35, 27);

        //no debe hacerse patron java beans, tiene ciertas limitantes
        NutritionsFactsJavaBeans cocaCola2 = new NutritionsFactsJavaBeans();
        cocaCola2.setServingSize(240);
        cocaCola2.setServings(8);
        cocaCola2.setCalories(100);
        cocaCola2.setSodium(35);
        cocaCola2.setCarbohydrate(27);

        //Patron builder recomendado
        NutritionFactsBuilder cocaCola3 = new
                NutritionFactsBuilder.Builder(240, 8)
                .calories(100).sodium(35).carbohydrate(27).build();

    }

}
