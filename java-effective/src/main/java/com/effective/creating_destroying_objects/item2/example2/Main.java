package com.effective.creating_destroying_objects.item2.example2;



public class Main {

    /**
     *  Consider a builder when faced with many
     * constructor parameters
     * @param args
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.init();
    }

    public void init(){
        NyPizza pizza = new NyPizza.Builder(NyPizza.Size.SMALL)
                .addTopping(Pizza.Topping.SAUSAGE).addTopping(Pizza.Topping.ONION).build();
        Calzone calzone = new Calzone.Builder()
                .addTopping(Pizza.Topping.HAM).sauceInside().build();

    }

}
