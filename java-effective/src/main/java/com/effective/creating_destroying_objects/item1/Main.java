package com.effective.creating_destroying_objects.item1;

import java.math.BigInteger;
import java.time.Clock;
import java.time.Instant;
import java.util.Date;
import java.util.EnumSet;

public class Main {

    /**
     *  Item 1: Consider static factory methods instead of
     * constructors - Prefer Dependecies Inyection
     * @param args
     */
    public static Integer count = 6;

    public static void main(String[] args) {
        //#1
        Boolean desicion =  Main.valueOf(true);
        System.out.println(desicion.getClass().getName());
        //#2
        for (String name: new String[] {"Foo", "Bareeee", "Bazzzzzzz"}) {
            new Thread(getRunnable(name,1000*name.length())).start();
        }
        System.out.println("init "+count);

        Date d = Date.from(Instant.now(Clock.systemDefaultZone()));
        System.out.println(d.toInstant());


    }

    public static Boolean valueOf(boolean b) {
        return b ? Boolean.TRUE : Boolean.FALSE;
    }


    public static Runnable getRunnable(final String name, final Integer time) {
        return () -> longTask(name, time);
    }

    public static void longTask(String label, Integer time) {
        System.out.println(label + ": start");
        try {
            Thread.sleep(time);
            Main.count -= 1;
            System.out.println(label+" "+count);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(label + ": end");
    }

}
