package com.effective.creating_destroying_objects.item3;

public class Main {

    /**
     * Enforce the singleton property with a private
     * constructor or an enum type
     *
     * @param args
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.init();
    }

    public void init() {
        Elvis.INSTANCE.leaveTheBuilding();
    }

}

// Singleton with public final field
class ElvisSingletonFinalField {
    public static final ElvisSingletonFinalField INSTANCE = new ElvisSingletonFinalField();

    private ElvisSingletonFinalField() {
    }

    public void leaveTheBuilding() {
    }
}

// Singleton with static factory
class ElvisStaticFactory {
    private static final ElvisStaticFactory INSTANCE = new ElvisStaticFactory();

    private ElvisStaticFactory() {
    }

    public static ElvisStaticFactory getInstance() {
        return INSTANCE;
    }

    public void leaveTheBuilding() {
    }

    // readResolve method to preserve singleton property
    private Object readResolve() {
        // Return the one true Elvis and let the garbage  collector
        // take care of the Elvis impersonator.
        return INSTANCE;
    }
}

// Enum singleton - the preferred approach
enum Elvis {
    INSTANCE;
    public void leaveTheBuilding() {
        System.out.println("Elvis will leave de builder");
    }
}