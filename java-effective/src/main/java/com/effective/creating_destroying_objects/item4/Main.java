package com.effective.creating_destroying_objects.item4;

public class Main {

    /**
     * Enforce noninstantiability with a private
     * constructor
     *
     * @param args
     */
    public static void main(String[] args) {
        Main main = new Main();
        main.init();
    }

    public void init() {

    }

}

// Noninstantiable utility class
class UtilityClass {
    // Suppress default constructor fornoninstantiability
    private UtilityClass() {
        throw new AssertionError();
    }
 // Remainder omitted
}
