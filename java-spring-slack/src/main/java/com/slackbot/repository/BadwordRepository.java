package com.slackbot.repository;

import com.slackbot.models.BadWord;
import org.springframework.data.repository.CrudRepository;

public interface BadwordRepository extends CrudRepository<BadWord, String>{
}
