package demo.feign;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import demo.dto.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@FeignClient(name = "PersonServer", fallback = PersonFeignFallback.class)
public interface IPersonFeign {

    @RequestMapping(value = "/person/{id}", method = GET)
    String helloPerson(@PathVariable("id") String id);

    @GetMapping("/user")
    ResponseEntity<ResponseData> getUsers();


}

