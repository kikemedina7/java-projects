package demo.feign;

import demo.dto.MetaData;
import demo.dto.ResponseData;
import demo.dto.UserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class PersonFeignFallback implements IPersonFeign{
    @Override
    public String helloPerson(String id) {
        return "Error con el server";
    }

    @Override
    public ResponseEntity<ResponseData> getUsers() {
        return ResponseEntity.ok().build();
    }
}
