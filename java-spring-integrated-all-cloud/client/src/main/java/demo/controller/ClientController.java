package demo.controller;

import demo.dto.ResponseData;
import demo.feign.IPersonFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class ClientController implements IPersonFeign {

    @Autowired
    private IPersonFeign personService;

    @RequestMapping(value = "/person/{id}", method = GET)
    public String helloPerson(@PathVariable String id) {
        return personService.helloPerson(id);
    }

    @GetMapping("/user")
    public ResponseEntity<ResponseData> getUsers() {
        return personService.getUsers();
    }
}
