package demo.service;


import demo.dto.UserDto;
import demo.exception.NoContentException;

import java.util.List;

public interface IUserService {

    List<UserDto> getUsers() throws NoContentException;

    UserDto getUser(Long id) throws NoContentException;

    UserDto createUser(UserDto user);

    UserDto updateUser(UserDto user);

    void deleteUser(UserDto user);

    UserDto findUserByUsername(String username) throws NoContentException;

}
