package demo.constant;

public interface ResourceMapping {
    String BASE_PERSON = "/person";

    String USER = "/user";
    String EXPORT = "/export";
    String USER_BY_USERNAME = "/userbyUsername";
    String HELLO_PERSON = "/{id}";
}
