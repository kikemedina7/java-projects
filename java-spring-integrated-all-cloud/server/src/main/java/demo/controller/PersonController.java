package demo.controller;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import demo.constant.ResourceMapping;
import demo.delegate.IDelegateController;
import demo.dto.ResponseData;
import demo.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class PersonController {

    @Autowired
    DiscoveryClient client;

    @Autowired
    private IDelegateController delegateController;

    @RequestMapping(value = "/person/{id}", method = GET)
    public ResponseEntity helloPerson(@PathVariable("id") String id) throws Exception {
        List<ServiceInstance> instances = client.getInstances("PersonServer");
        ServiceInstance selectedInstance = instances
                .get(new Random().nextInt(instances.size()));
        return ResponseEntity.badRequest().body("Error");
    }

    @GetMapping(ResourceMapping.USER)
    public ResponseEntity<ResponseData> getUsers() {
        return delegateController.getUsers();
    }

    @PostMapping(ResourceMapping.USER)
    public ResponseEntity createUser(@RequestBody UserDto userDto) {
        return delegateController.createUser(userDto);
    }

    @PutMapping(ResourceMapping.USER + "/{id}")
    public ResponseEntity updateUser(@PathVariable Long id, @RequestBody UserDto userDto) {
        return delegateController.updateUser(id, userDto);
    }

    @DeleteMapping(ResourceMapping.USER + "/{id}")
    public ResponseEntity deleteUser(@PathVariable Long id) {
        return delegateController.deleteUser(id);
    }

    @GetMapping(value = ResourceMapping.USER + ResourceMapping.EXPORT, produces = "text/csv")
    public void getUsersCSV(HttpServletResponse response) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        String filename = "users.csv";
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + filename + "\"");
        StatefulBeanToCsv<UserDto> writer = new StatefulBeanToCsvBuilder<UserDto>(response.getWriter())
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withOrderedResults(false)
                .build();

        writer.write((List<UserDto>) Objects.requireNonNull(delegateController.getUsers().getBody()));
    }

    @GetMapping(ResourceMapping.USER_BY_USERNAME+"/{username}")
    public ResponseEntity findByUsername(@PathVariable String username){
        return this.delegateController.findByUsername(username);
    }
}
