package demo.mapper;


import demo.domain.User;
import demo.dto.UserDto;
import org.mapstruct.*;

import java.util.List;


@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE,
        componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface IUserMapper extends IMapperGeneric<User, UserDto> {

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "userEmail", target = "userPrincipalEmail")
    @Override
    User dtoToEntity(UserDto dto);

    @Override
    @Mapping(target = "id", ignore = true)
    @Mapping(source = "userPrincipalEmail", target = "userEmail")
    @Mapping(source = "userStartDate", target = "userStartDate",dateFormat = "yyyy-MM-dd")
    UserDto entityToDto(User entity);

    @Override
    List<UserDto> entityToDto(List<User> entity);

    @Override
    List<User> dtoToEntity(List<UserDto> dto);

    @Override
    void mergeToEntity(Object entityOld, @MappingTarget User entityNew);

    @Override
    void mergeToDtos(Object dtoOld, @MappingTarget UserDto dtoNew);


}

