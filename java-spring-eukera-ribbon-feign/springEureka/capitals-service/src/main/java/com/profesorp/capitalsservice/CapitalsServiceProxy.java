package com.profesorp.capitalsservice;

import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;



@FeignClient(name="countries-service", fallbackFactory = HystrixClientFallbackFactory.class)

public interface CapitalsServiceProxy {
	@GetMapping("/{country}")
	public CapitalsBean getCountry(@PathVariable("country") String country);
}


@Component
class HystrixClientFallbackFactory implements FallbackFactory<CapitalsServiceProxy> {

	private Logger log = LoggerFactory.getLogger(HystrixClientFallbackFactory.class);

	@Override
	public CapitalsServiceProxy create(Throwable cause) {
		return new CapitalsServiceProxy() {

			@Override
			public CapitalsBean getCountry(String country) {
				log.error(cause.getMessage());
				return new CapitalsBean().builder().capital("NO HAY").country("NO HAY").name("TAGOTAO").port(1000).build();
			}
		};
	}
}