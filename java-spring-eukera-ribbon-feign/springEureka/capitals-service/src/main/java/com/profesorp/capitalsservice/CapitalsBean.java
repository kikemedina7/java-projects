package com.profesorp.capitalsservice;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CapitalsBean {
	private String country;	
	private String name;
	private String capital;
	private int port;	
}
