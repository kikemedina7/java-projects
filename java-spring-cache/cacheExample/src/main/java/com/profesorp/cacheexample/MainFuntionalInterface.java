package com.profesorp.cacheexample;

import com.profesorp.cacheexample.dtos.DtoRequest;
import com.profesorp.cacheexample.interfaces.OperacionBinaria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

import static java.lang.Thread.sleep;

class MainFuntionalInterface {

    private static Logger logger = LoggerFactory.getLogger(MainFuntionalInterface.class);

    public static void main(String[] args) {
        OperacionBinaria res = (x, y) -> {
            DtoRequest rq = new DtoRequest();
            Integer z = (x*y);
            return " El valor es : "+z;
        };


        System.out.println(res.binaryOperation(5,8));

        Runnable task = () -> System.out.println("Hola mundo");
        try {
            sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}