package com.profesorp.cacheexample.interfaces;

@FunctionalInterface
public interface OperacionBinaria {
  String binaryOperation(Integer x, Integer y);
}
