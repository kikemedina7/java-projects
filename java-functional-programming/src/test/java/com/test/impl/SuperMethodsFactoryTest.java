package com.test.impl;

import com.test.dtos.Product;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class SuperMethodsFactoryTest {

    @Test
    void main() {

        Optional<List<Product>> producto = Optional.of(Arrays.asList(
                Product.builder().name("Prod1").build(),
                Product.builder().name("Prod2").build(),
                Product.builder().name("Prod3").build(),
                Product.builder().name("Prod4").build()
        ));


        assertEquals(Boolean.TRUE,Objects.nonNull(producto));
    }
}