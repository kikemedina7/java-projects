package com.test.interfaces;

@FunctionalInterface
public interface ConsumeFactory<T,K> {
    K reduce(T param1, T param2);
}
