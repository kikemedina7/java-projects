package com.test.impl;

import com.test.dtos.Customer;
import com.test.dtos.Product;
import com.test.interfaces.ConsumeFactory;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SuperMethodsFactory {


    public static void main(String[] args) {

        new SuperMethodsFactory();

    }

    public SuperMethodsFactory(){

//       Consume Functional interface Custom
        System.out.println("\n---------------- consume Functional interface Custom");
        ConsumeFactory<List<String>,String> listFactory = (List<String> listA,List<String> listB) ->  listA.get(0);
        System.out.println(listFactory.reduce(Arrays.asList("Hola 1","Que hace"),Arrays.asList("Hola 2","Que hace")));

        List<Product> listProd = Arrays.asList(
                Product.builder().name("TDC AMEX").price(new BigDecimal(10000)).type("TDC").build(),
                Product.builder().name("TDC VISA").price(new BigDecimal(200000)).type("TDC").build(),
                Product.builder().name("TDC MASTERCARD").price(new BigDecimal(35000)).type("TDC").build(),
                Product.builder().name("ACCOUNT PREMIUM").price(new BigDecimal(5)).type("ACCOUNT").build()
        );

        List<Customer> listcustomer = Arrays.asList(
                Customer.builder().name("Jorge").lastName("Medina").products(listProd).build(),
                Customer.builder().name("Pepe").lastName("Mendoza").products(listProd).build(),
                Customer.builder().name("Alfonso").lastName("Perez").products(listProd).build(),
                Customer.builder().name("Sonelis").lastName("Martinez").products(listProd).build()
        );
//      Uso de interfaz funcional function de java.util.function
        System.out.println("\n---------------- Uso de interfaz funcional function de java.util.function");
        Function<List<Customer>,List<String>> getName = (list) -> list.stream().filter(item -> item.getLastName().startsWith("M")).map(Customer::getName).collect(Collectors.toList());

        getName.apply(listcustomer).forEach(System.out::println);

        //      Uso de interfaz funcional function de java.util.function
        System.out.println("\n---------------- Uso de interfaz funcional function de java.util.function");
        BiFunction<List<Customer>,StringBuilder,String> customerStringStreams = (listCust,sBuilder) -> {
            sBuilder.append(String.join(",",listCust.stream().map(Customer::getName).collect(Collectors.toList())));
            return sBuilder.toString();
        };

        System.out.println(customerStringStreams.apply(listcustomer, new StringBuilder()));
        //      Uso de interfaz funcional function de java.util.function
        System.out.println("\n---------------- Uso de interfaz funcional BiPredicate de java.util.function");

        BiPredicate<List<Customer>,String> validCus = (list,valFilter) -> list.stream().filter(item -> item.getLastName().startsWith(valFilter)).count() > 0L;

        System.out.println(validCus.test(listcustomer, "M"));

        //      Uso de interfaz funcional function de java.util.function
        System.out.println("\n---------------- Uso de interfaz consumer (consumidora) function de java.util.function");
        Consumer<List<Customer>> supp = (lst) -> lst.stream()
                .flatMap(pList -> pList.getProducts().stream()).forEach(System.out::println);

        supp.accept(listcustomer);

        //      Uso de interfaz funcional function de java.util.function
        System.out.println("\n---------------- Uso de interfaz supplier (proveedora) function de java.util.function");
        Supplier<List<Customer>> listaModcustomers = () -> {
            List<Product> listPro1 = Arrays.asList(
                    Product.builder().name("TDC AMEX").price(new BigDecimal(10000)).type("TDC").build(),
                    Product.builder().name("TDC VISA").price(new BigDecimal(200000)).type("TDC").build(),
                    Product.builder().name("TDC MASTERCARD").price(new BigDecimal(35000)).type("TDC").build(),
                    Product.builder().name("ACCOUNT BASIC").price(new BigDecimal(78)).type("ACCOUNT2").build(),
                    Product.builder().name("ACCOUNT BASIC").price(new BigDecimal(78)).type("ACCOUNT1").build()
            );
            List<Product> listPro2 = Arrays.asList(
                    Product.builder().name("TDC MASTERCARD").price(new BigDecimal(1000)).type("TDC").build(),
                    Product.builder().name("TDC MASTERCARD").price(new BigDecimal(200)).type("TDC").build(),
                    Product.builder().name("ACCOUNT PREMIUM").price(new BigDecimal(78)).type("ACCOUNT1").build(),
                    Product.builder().name("ACCOUNT SUITE").price(new BigDecimal(78)).type("ACCOUNT1").build(),
                    Product.builder().name("TDD PREMIUM").price(new BigDecimal(3000000)).type("TDD").build()
            );

            List<Customer> listcustom = Arrays.asList(
                    Customer.builder().identityNumber(123).name("Albert").lastName("Stanton").products(listPro1).build(),
                    Customer.builder().identityNumber(456).name("Pepe").lastName("Mendoza").products(listPro2).build()

            );
            return listcustom;
        };
        listaModcustomers.get().stream().forEach(System.out::println);

        //      Uso de interfaz funcional function de java.util.function
        System.out.println("\n---------------- Uso de Iterate ");
        BiFunction<Integer,Integer, IntStream> sInts = (size,limit) -> {
            return IntStream.iterate(3,n -> n + 1).limit(limit);
        };

        sInts.apply(3,12).forEachOrdered(System.out::println);

        //      Uso de interfaz funcional function de java.util.function
        System.out.println("\n---------------- Uso de Iterate ");
        Stream.of(1,2,3)
                .min(Comparator.naturalOrder())
                .ifPresent(integer -> System.out.println(integer));

        System.out.println("\n---------------- Uso de colectors: joining ");
        String value = listaModcustomers.get().stream()
                .flatMap(cust -> cust.getProducts().stream())
                .map(prod -> prod.getName())
                .collect(Collectors.joining(",","(",")"));
        System.out.println(value);

        System.out.println("\n---------------- Uso de colectors: toSet ");
        Set<Product> setProduct = listaModcustomers.get().stream()
                .flatMap(cust -> cust.getProducts().stream())
                .peek(prod -> {
                    System.out.println(prod);
                })
                .collect(Collectors.toSet());

        System.out.println("\n---------------- Uso de SET ------------------- No admite duplicados" +
                " Orden de velocidad: HashSet, LinkedHashSet, TreeSet" );

        System.out.println("\n---------------- Uso de colectors: to Custom collection TreeSet" +
                "\n implementa intefraz comparable, donde se indica con que campo se hara el ordenamiento");
        listaModcustomers.get().stream()
                .flatMap(cust -> cust.getProducts().stream())
                .collect(Collectors.toCollection(TreeSet::new))
                .forEach(System.out::println);

        System.out.println("\n---------------- Uso de colectors: to Custom collection HashSet" +
                "\n no garantiza ningun orden");
        listaModcustomers.get().stream()
                .flatMap(cust -> cust.getProducts().stream())
                .collect(Collectors.toCollection(HashSet::new))
                .forEach(System.out::println);

        System.out.println("\n---------------- Uso de colectors: to Custom collection LinkedHashSet" +
                "\n almacena en funcion del orden de insercion");
        listaModcustomers.get().stream()
                .flatMap(cust -> cust.getProducts().stream())
                .collect(Collectors.toCollection(LinkedHashSet::new))
                .forEach(System.out::println);

        System.out.println("\n---------------- Uso de colectors: to Custom collection Map");
        Map<BigDecimal, Product> mapper = listaModcustomers.get().stream()
                .flatMap(cust -> cust.getProducts().stream())
                .filter(fil -> fil.getType().equals("TDC"))
                .collect(Collectors.toMap(Product::getPrice, Function.identity())); // Function.identity()- lo que recibe lo retorna
        System.out.println(mapper);

        System.out.println("\n---------------- Uso de colectors: to Custom collection Map Evolve con fusion de objectos iguales");
        mapper = listaModcustomers.get().stream()
                .flatMap(cust -> cust.getProducts().stream())
                .filter(fil -> fil.getType().equals("ACCOUNT"))
                .collect(Collectors.toMap(
                        Product::getPrice
                        ,Function.identity()
                        ,(prod1,prod2) -> {
                            return Product.builder()
                                    .name(prod1.getName())
                                    .price(prod1.getPrice().add(prod2.getPrice()))
                                    .type(prod1.getType()+" + "+prod2.getType())
                                    .build();
                        }
                )); // Function.identity()- lo que recibe lo retorna
        System.out.println(mapper);

        System.out.println("\n---------------- Uso de colectors: to Custom collection Map Evolve con fusion de objectos iguales y devolucion de tipo de mapa");
        mapper = listaModcustomers.get().stream()
                .flatMap(cust -> cust.getProducts().stream())
                .filter(fil -> fil.getType().equals("ACCOUNT"))
                .collect(Collectors.toMap(
                        Product::getPrice
                        ,Function.identity()
                        ,(prod1,prod2) -> {
                            return Product.builder()
                                    .name(prod1.getName())
                                    .price(prod1.getPrice().add(prod2.getPrice()))
                                    .type(prod1.getType()+" + "+prod2.getType())
                                    .build();
                        }
                        ,TreeMap::new
                        )
                );
        System.out.println(mapper);

        System.out.println("\n---------------- Uso de Stream/ arrays:");
        Product[] arraysObj = listaModcustomers.get().stream()
                .flatMap(cust -> cust.getProducts().stream())
//                .toArray(size -> new Product[size]);
                .toArray(Product[]::new);
        Arrays.stream(arraysObj).forEach(System.out::println);

        System.out.println("\n---------------- Uso de Stream Collector grouping (USER EN VEZ DE MAP)");
        Map<BigDecimal, List<Product>> groupMap = listaModcustomers.get().stream()
                .flatMap(item -> item.getProducts().stream())
                .collect(Collectors.groupingBy(Product::getPrice)); // Mejor que map
        groupMap.entrySet().stream().forEach(System.out::println);

        System.out.println("\n---------------- Uso de Stream Collector grouping (USER EN VEZ DE MAP) 2 arg. correspondiente a un downStreamCollector" +
                "\n el cual ayudara a determinar cual es el collector que se usara");
        Map<BigDecimal, String> groupMap2 = listaModcustomers.get().stream()
                .flatMap(item -> item.getProducts().stream())
                .collect(Collectors.groupingBy(
                        Product::getPrice
                        , Collectors.mapping(Product::getName, Collectors.joining(","))
                        )
                ); // Mejor que map
        groupMap2.entrySet().stream().forEach(System.out::println);

        System.out.println("\n---------------- Uso de Stream Collector grouping (USER EN VEZ DE MAP) 2 arg. correspondiente a un downStreamCollector" +
                "\n el cual ayudara a determinar cual es el collector que se usara, 3er tipo para indicar que es un objecto que uno indique, en este caso un TreeMap");
        TreeMap<BigDecimal, String> groupMap3 = listaModcustomers.get().stream()
                .flatMap(item -> item.getProducts().stream())
                .collect(Collectors.groupingBy(
                        Product::getPrice // Agrupador / Classifier
                        , TreeMap::new // Supplier
                        , Collectors.mapping(Product::getName, Collectors.joining(",")) // downStreamCollector
                        )
                ); // Mejor que map
        groupMap2.entrySet().stream().forEach(System.out::println);

        System.out.println("\n---------------- Uso de Stream Collector grouping con Colectores basicos : counting como downStreamCollectors");
        Map<String, Long> groupMap4 = listaModcustomers.get().stream()
                .flatMap(item -> item.getProducts().stream())
                .collect(Collectors.groupingBy(Product::getName, Collectors.counting()));
        System.out.println(groupMap4);

        System.out.println("\n---------------- Uso de Stream Collector grouping con Colectores basicos : summingInt como downStreamCollectors");
        Map<String, Integer> groupMap5 = listaModcustomers.get().stream()
                .flatMap(item -> item.getProducts().stream())
                .collect(Collectors.groupingBy(Product::getName, Collectors.summingInt(item -> Integer.valueOf(item.getPrice().intValue()))));
        System.out.println(groupMap5);

        System.out.println("\n---------------- Uso de Stream Collector grouping con Colectores basicos : minBy como downStreamCollectors");
        Map<String, Optional<Product>> groupMap6 = listaModcustomers.get().stream()
                .flatMap(item -> item.getProducts().stream())
                .collect(Collectors.groupingBy(
                        Product::getType
                        , Collectors.minBy(
                                Comparator.comparing(Product::getPrice) // Mejor Opcion para comparar
//                                 Product::compareTo
                        )));
        groupMap6.entrySet().stream().forEach(System.out::println);

        System.out.println("\n---------------- Uso de Stream Collector grouping con Colectores basicos : maxBy como downStreamCollectors");
        Map<String, Optional<Product>> groupMap7 = listaModcustomers.get().stream()
                .flatMap(item -> item.getProducts().stream())
                .collect(Collectors.groupingBy(
                        Product::getType
                        , Collectors.maxBy(
                                Comparator.comparing(Product::getPrice) // Mejor Opcion para comparar
//                                 Product::compareTo
                        )));
        groupMap7.entrySet().stream().forEach(System.out::println);

        System.out.println("\n---------------- Uso de Stream Collector grouping con Colectores complejos Summarizing (cuenta, suma, minimo, maximo, promedio)");
        Map<String, IntSummaryStatistics> groupMap8 = listaModcustomers.get().stream()
                .flatMap(item -> item.getProducts().stream())
                .collect(Collectors.groupingBy(
                        Product::getType
                        , Collectors.summarizingInt(item -> Integer.valueOf(item.getPrice().intValue())
                        )));
        groupMap8.entrySet().stream().forEach(System.out::println);

        System.out.println("\n---------------- Uso de Stream Collector con particion" +
                "\n parecido un groupingBy sin embargo sera evaluaado con un predicado, por lo tanto geenrara dos grupos siempre");
        Map<Boolean, List<Product>> groupMap9 = listaModcustomers.get().stream()
                .flatMap(item -> item.getProducts().stream())
                .collect(Collectors.partitioningBy(item -> item.getType().equals("ACCOUNT")));
        groupMap9.entrySet().stream()
//                .filter(val -> val.getKey())
                .forEach(System.out::println);

        System.out.println("\n---------------- Uso de Stream Collector con particion y un downStreamCollection" +
                "\n parecido un groupingBy sin embargo sera evaluaado con un predicado, por lo tanto geenrara dos grupos siempre");
        Map<Boolean, Long> groupMap10 = listaModcustomers.get().stream()
                .flatMap(item -> item.getProducts().stream())
                .collect(Collectors.partitioningBy(item -> item.getType().equals("ACCOUNT"), Collectors.counting()));
        groupMap10.entrySet().stream()
//                .filter(val -> val.getKey())
                .forEach(System.out::println);

        System.out.println("\n---------------- Ejercicio Uso de Stream Collector Cuantos registros hay de cada tipo de producto y que su precio sea < 20000 " +
                "\n en java 8 no esta Filtering, asi que se tiene que crear un metodo generico que supla la necesidad SuperMethodsFactory.filtering");
        Map<String, Long> groupMap11 = listaModcustomers.get().stream()
                .flatMap(item -> item.getProducts().stream())
                .collect(Collectors.groupingBy(Product::getType, SuperMethodsFactory.filtering(item -> item.getPrice().intValue() < 20000,Collectors.counting())));
        groupMap11.entrySet().stream()
                .forEach(System.out::println);

        System.out.println("\n---------------- Uso de Stream Comparador Avanzado");
        listaModcustomers.get().stream()
                .flatMap(item -> item.getProducts().stream())
//                .sorted((prod1,prod2) -> prod1.getPrice().intValue() - prod2.getPrice().intValue()) // Compacion por precio
//                .sorted((prod1,prod2)-> Integer.compare(prod1.getPrice().intValue(),prod2.getPrice().intValue())) // Comparacion por precio
//                .sorted((prod1,prod2)-> prod1.getName().compareTo(prod2.getName()))// Comparacion por nombre
//                .sorted(Comparator.naturalOrder())// implementa la interfaz comparable el Objeto Producto
//                 .sorted(Product::compareTo)// implementa la interfaz comparable el Objeto Producto
//                .sorted(Comparator.reverseOrder())// implementa la interfaz comparable el Objeto Producto  reversando el orden
                /*            .sorted((prod1,prod2)-> {
                    int nameProductComp = prod1.compareTo(prod2);
                    if(nameProductComp != 0){
                        return nameProductComp;
                    }else{
                        return prod1.getPrice().intValue() - prod2.getPrice().intValue();
                    }
                })*///usando doble filtro el comparre to que ordena por nombre y luego ordenando por precio
//                .sorted(Comparator.comparing(Product::getName))//KeyExtractor filtrando por nombre
//                .sorted(Comparator.comparing(Product::getName,Comparator.reverseOrder()))//KeyExtractor filtrando por nombre y luego se ordena de forma reversa
//                .sorted(Comparator.comparing(Product::getName).thenComparing(Product::getPrice))//KeyExtractor usando thencomparing filtrando por nombre y luego por el precio
//                .sorted(Comparator.comparing(Product::getName).thenComparing(Product::getPrice,Comparator.reverseOrder()))//KeyExtractor usando thencomparing filtrando por nombre y luego por el precio que se ordena de forma reversa
//                .sorted(Comparator.comparing(Product::getName, Comparator.reverseOrder())
//                        .thenComparing(Product::getPrice,Comparator.reverseOrder()))//KeyExtractor usando thencomparing filtrando por nombre de forma reversa y luego por el precio que se ordena de forma reversa
                .sorted(Comparator
                        .comparing(Product::getName, Comparator.reverseOrder())
                        .thenComparing(Product::getPrice,Comparator.reverseOrder())
                        .thenComparing(Product::getType,Comparator.reverseOrder()))//KeyExtractor usando thencomparing filtrando por nombre de forma reversa y luego por el precio que se ordena de forma reversa
                .forEach(System.out::println);

        System.out.println("\n---------------- Uso de Stream : Optional y map de optional");
        Product opMapOptional = listaModcustomers.get().stream()
                .flatMap(customer -> customer.getProducts().stream())
                .sorted(Comparator
                        .comparing(Product::getName, Comparator.reverseOrder())
                        .thenComparing(Product::getPrice, Comparator.reverseOrder())
                        .thenComparing(Product::getType, Comparator.reverseOrder()))
                .findFirst()
                .orElseGet(() -> Product.builder().name("No hay producto").type("No hay tipo").price(new BigDecimal(0)).build());
        System.out.println(opMapOptional);

        System.out.println("\n---------------- Uso de Stream : Optional y stream de optional ( JAVA 9 ) se muestra como hacelro en java 8");
        List<String> typeproduct = Arrays.asList("ACCOUNT1","ACCOUNT2");
       /* typeproduct.stream()
                .map(this::findProductForType)
                .flatMap(Optional::stream)
                .collect(Collectors.toList())
                .forEach(System.out::println);*/
       //For java 8
        System.out.println("\n Java 8 :");
        List<Product> filterProd = typeproduct.stream()
                .map(this::findProductForType)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
        filterProd.forEach(System.out::println);

        System.out.println("--------------------- Uso Stream colector collectingAndThen que primero recolecta y despues filtra." +
                "\n a diferencia de Collectors.mapping que primero filtrar y luego recolecta");
        String resValue = listaModcustomers.get().stream()
                .collect(Collectors.collectingAndThen(Collectors.counting(), aLong -> aLong + " Customers"));
        System.out.println(resValue);

        System.out.println("--------------------- Uso Stream colector groupingBy que primero recolecta y despues filtra." +
                "\n a diferencia de Collectors.mapping que primero filtrar y luego recolecta");
        Map<String, String> resValue2 = listaModcustomers.get().stream()
                .flatMap(customer -> customer.getProducts().stream())
                .collect(Collectors.groupingBy(
                        Product::getType
                        , Collectors.collectingAndThen(
                                Collectors.counting()
                                , aLong -> aLong + " Products"
                        )));
        System.out.println(resValue2);

        System.out.println("--------------------- JAVA 12 : Uso Stream colector Ccollectors.teeing");
        System.out.println("--------------------- Uso Streams : Filtrar y ordenar trras recolectar mapa");

        TreeMap<String, Long> resMapAvanced = listaModcustomers.get().stream()
                .flatMap(customer -> customer.getProducts().stream())
                .collect(Collectors.groupingBy(Product::getType, Collectors.counting()))
                .entrySet().stream()
                .filter(stringLongEntry -> stringLongEntry.getValue() > 1)
                .sorted(Map.Entry.comparingByValue(Comparator.naturalOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (entry1, entry2) -> entry1, TreeMap::new));
        System.out.println(resMapAvanced);

    }

    static <T, A, R> Collector<T, A, R> filtering(
            Predicate<? super T> filter, Collector<T, A, R> collector) {
        return Collector.of(
                collector.supplier(),
                (accumulator, input) -> {
                    if (filter.test(input)) {
                        collector.accumulator().accept(accumulator, input);
                    }
                },
                collector.combiner(),
                collector.finisher());

//        SuperMethodsFactory.filtering(item -> item.getPrice().intValue() < 20000,Collectors.counting())
    }

    Optional<Product> findProductForType(String type){
        Supplier<List<Customer>> listaModcustomers = () -> {
            List<Product> listPro1 = Arrays.asList(
                    Product.builder().name("TDC AMEX").price(new BigDecimal(10000)).type("TDC").build(),
                    Product.builder().name("TDC VISA").price(new BigDecimal(200000)).type("TDC").build(),
                    Product.builder().name("TDC MASTERCARD").price(new BigDecimal(35000)).type("TDC").build(),
                    Product.builder().name("ACCOUNT BASIC").price(new BigDecimal(78)).type("ACCOUNT2").build(),
                    Product.builder().name("ACCOUNT BASIC").price(new BigDecimal(78)).type("ACCOUNT1").build()
            );
            List<Product> listPro2 = Arrays.asList(
                    Product.builder().name("TDC MASTERCARD").price(new BigDecimal(1000)).type("TDC").build(),
                    Product.builder().name("TDC MASTERCARD").price(new BigDecimal(200)).type("TDC").build(),
                    Product.builder().name("ACCOUNT PREMIUM").price(new BigDecimal(78)).type("ACCOUNT1").build(),
                    Product.builder().name("ACCOUNT SUITE").price(new BigDecimal(78)).type("ACCOUNT1").build(),
                    Product.builder().name("TDD PREMIUM").price(new BigDecimal(3000000)).type("TDD").build()
            );

            return Arrays.asList(
                    Customer.builder().identityNumber(123).name("Albert").lastName("Stanton").products(listPro1).build(),
                    Customer.builder().identityNumber(456).name("Pepe").lastName("Mendoza").products(listPro2).build()

            );
        };
        return listaModcustomers.get().stream()
                .flatMap(customer -> customer.getProducts().stream())
                .filter(product -> product.getType().equals(type))
                .findFirst();
    }


}
