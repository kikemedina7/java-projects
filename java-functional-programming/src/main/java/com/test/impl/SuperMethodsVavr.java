package com.test.impl;

import com.test.dtos.Person;
import com.test.dtos.PersonValidator;
import io.vavr.*;
import io.vavr.collection.*;
import io.vavr.concurrent.Future;
import io.vavr.control.Either;
import io.vavr.control.Option;
import io.vavr.control.Try;
import io.vavr.control.Validation;

import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Function;

public class SuperMethodsVavr {

    public static void main(String[] args) {
        new SuperMethodsVavr();
    }

    public SuperMethodsVavr(){

/*        //Lista
        System.out.println("\n---------------------- Vavr List ");
        List<Integer> list1 = List.of(1, 2, 3);
        list1.toStream().forEach(System.out::println);
        List<Integer> list2 = list1.tail().prepend(0);
        list2.toStream().forEach(System.out::println);

        System.out.println("\n---------------------- Vavr cola Queue ");

        Queue<Integer> queue = Queue.of(1, 2, 3)
                .enqueue(4)
                .enqueue(5);

        System.out.println("\n--- Vavr cola Queue : Tuple2");
        Queue<Integer> queue2 = Queue.of(1, 2, 3);

        // = (1, Queue(2, 3))
        Tuple2<Integer, Queue<Integer>> dequeued =
                queue2.dequeue();

        System.out.println("\n--- Vavr cola Queue : dequeueOption");
        // = Some((1, Queue()))
        Queue.of(1).dequeueOption();

        System.out.println("\n--- Vavr cola Queue : empty");
        // = None
        Queue.empty().dequeueOption();

        // = Queue(1)
        Queue<Integer> queue3 = Queue.of(1);

        System.out.println("\n--- Vavr cola Queue : Tuple2 with option");
        // = Some((1, Queue()))
        Option<Tuple2<Integer, Queue<Integer>>> dequeued2 =
                queue3.dequeueOption();

        System.out.println("\n--- Vavr cola Queue : dequeued with map");
        // = Some(1)
        Option<Integer> element = dequeued2.map(Tuple2::_1);

        // = Some(Queue())
        Option<Queue<Integer>> remaining =
                dequeued2.map(Tuple2::_2);

        System.out.println("\n---------------------- Vavr order set (TreeSet) ");
        // = TreeSet(1, 2, 3);
        SortedSet<Integer> set = TreeSet.of(2, 3, 1, 2);

        // = TreeSet(3, 2, 1);
        Comparator<Integer> c = (a, b) -> b - a;
        SortedSet<Integer> reversed = TreeSet.of(c, 2, 3, 1, 2);

        // = Stream("1", "2", "3") in Vavr
        Stream.of(1, 2, 3).forEach(System.out::println);

        // (Java, 8)
        Tuple2<String, Integer> java8 = Tuple.of("Java", 8);

        // "Java"
        String s = java8._1;

        // 8
        Integer i = java8._2;

        // (vavr, 1)
        Tuple2<String, Integer> that = java8.map(
                o -> o.substring(2) + "vr",
                p -> p / 8
        );

        // (vavr, 1)
        Tuple2<String, Integer> that2 = java8.map(
                (v, k) -> Tuple.of(v.substring(2) + "vr", k / 8)
        );

        // "vavr 1"
        String that3 = java8.apply(
                (z, u) -> z.substring(2) + "vr " + u / 8
        );
        */

        System.out.println("\n--------------------- Initial Approach");
        System.out.println("\n--------------------- Tuple (Tuplas)");
        System.out.println("\n--- Tuple.of = fabrica hasta 8 tuplas inmutables.");

        Tuple2<String, Integer> bitTuple = Tuple.of("Java",8);

        // "Java"
        System.out.println(bitTuple._1);
        // 8
        System.out.println(bitTuple._2);

        System.out.println("\n--- Asignar una tupla por componentes");

        Tuple2<String, Integer> compTuple = bitTuple.map(
                s -> s.substring(2) + "vr",
                i -> i / 8
        );

        // "Java"
        System.out.println(compTuple._1);
        // 8
        System.out.println(compTuple._2);

        System.out.println("\n--- Mapear una tupla usando un mapeador");

        Tuple2<String, Integer> that = bitTuple.map(
                (s, i) -> Tuple.of(s.substring(2) + "vr", i / 8)
        );
        // vavr
        System.out.println(compTuple._1);
        // 1
        System.out.println(compTuple._2);

        System.out.println("\n--- Transformar una tupla");

        String trasformTuple = bitTuple.apply(
                (s, i) -> s.substring(2) + "vr " + i / 8
        );

        System.out.println(trasformTuple);
        System.out.println("\n--------------------- Funciones (Integerfaces funcionales personalizadas)");

        Function2<Integer,Integer,Integer> sum = (a,b) -> a+b;

        System.out.println(sum.apply(1,2));

        System.out.println("\n--- Crear funciones con referencias a metodos");

        Function3<String,String,String,String> funct3 = Function3.of(this::methodWhichAccepts3Parameters);

        System.out.println(funct3.apply("cesar","contreras","conmigo"));

        System.out.println("\n--- Composicion : Puede componer funciones. En matemáticas, la composición de funciones es la aplicación de una función al resultado de otra para producir una tercera función. Por ejemplo, las funciones f: X → Y y g: Y → Z se pueden componer para producir una función h: g(f(x))que mapee X → Z.");

        Function1<Integer, Integer> plusOne = a -> a + 1;
        Function1<Integer, Integer> multiplyByTwo = a -> a * 2;

        Function1<Integer, Integer> add1AndMultiplyBy2 = plusOne.andThen(multiplyByTwo);

        System.out.println(add1AndMultiplyBy2.apply(3));

        Function1<Integer, Integer> add1AndMultiplyBy2_2 = multiplyByTwo.compose(plusOne);

        System.out.println(add1AndMultiplyBy2_2.apply(3));
        System.out.println("\n--- Lifting o Levantamiento : Puede convertir una función parcial en una función total que devuelve un Optionresultado. El término función parcial proviene de las matemáticas. Una función parcial de X a Y es una función f: X ′ → Y, para algún subconjunto X ′ de X. Generaliza el concepto de una función f: X → Y al no forzar a f a mapear cada elemento de X a un elemento de Y. Eso significa que una función parcial funciona correctamente solo para algunos valores de entrada. Si se llama a la función con un valor de entrada no permitido, normalmente generará una excepción.");

        Function2<Integer, Integer, Integer> divide = (a, b) -> a / b;
        Function2<Integer, Integer, Option<Integer>> safeDivide = Function2.lift(divide);

        // = None
        Option<Integer> i1 = safeDivide.apply(1, 0);

        // = Some(2)
        Option<Integer> i2 = safeDivide.apply(4, 2);

        Function2<Integer, Integer, Option<Integer>> sum_1 = Function2.lift(this::sum);

        // = None
        Option<Integer> optionalResult = sum_1.apply(-1, 2);

        System.out.println("\n--- Partial Application : Puede convertir una función parcial en una función total que devuelve un Optionresultado. El término función parcial proviene de las matemáticas. Una función parcial de X a Y es una función f: X ′ → Y, para algún subconjunto X ′ de X. Generaliza el concepto de una función f: X → Y al no forzar a f a mapear cada elemento de X a un elemento de Y. Eso significa que una función parcial funciona correctamente solo para algunos valores de entrada. Si se llama a la función con un valor de entrada no permitido, normalmente generará una excepción.");

        Function5<Integer, Integer, Integer, Integer, Integer, Integer> sum1 = (a, b, c, d, e) -> a + b + c + d + e;
        Function2<Integer, Integer, Integer> add6 = sum1.apply(2, 3, 1);

        System.out.println(add6.apply(1,1));
        System.out.println("\n--- Currying / Zurra : Currying es una técnica para aplicar parcialmente una función fijando un valor para uno de los parámetros, lo que da como resultado una Function1función que devuelve un Function1.\n" +
                "\n" +
                "Cuando a Function2se curry , el resultado es indistinguible de la aplicación parcial de a Function2porque ambos dan como resultado una función de 1-aridad.");

        Function3<Integer, Integer, Integer, Integer> sum2 = (a, b, c) -> a + b + c;
        final Function1<Integer, Function1<Integer, Integer>> add2 = sum2.curried().apply(2);

        System.out.println(add2.apply(2).apply(3));

        System.out.println("\n--- Memoization / Memorizacion : La memorización es una forma de almacenamiento en caché. Una función memorizada se ejecuta solo una vez y luego devuelve el resultado de un caché.\n" +
                "El siguiente ejemplo calcula un número aleatorio en la primera invocación y devuelve el número almacenado en caché en la segunda invocación.");

        Function0<Double> hashCache =
                Function0.of(Math::random).memoized();

        double randomValue1 = hashCache.apply();
        double randomValue2 = hashCache.apply();

        System.out.println(randomValue1 == randomValue2 ? true : false);


        System.out.println("\n--------------------- Values (Integerfaces funcionales personalizadas)");

        System.out.println("\n--- Option");

        System.out.println("\n--- Usando Optional, este escenario es válido.");
        Optional<String> maybeFoo = Optional.of("foo");
        Optional<String> maybeFooBar = maybeFoo.map(s -> (String)null)
                .map(s -> s.toUpperCase() + "bar");

        System.out.println(maybeFooBar.isPresent());

        System.out.println("\n--- Usando Vavr Option, el mismo escenario resultará en un NullPointerException.");
        Option<String> maybeFoo1 = Option.of("foo");

        try {
            maybeFoo1.map(s -> (String)null)
                    .map(s -> s.toUpperCase() + "bar");
        } catch (NullPointerException e) {
            // this is clearly not the correct approach
            System.out.println("Es nulo = "+e.getMessage());
        }

        System.out.println("\n--- Usando Vavr Option, el mismo escenario resultará en un NullPointerException.");
        Option<String> maybeFoo2 = Option.of("foo");
        Option<String> maybeFooBar2 = maybeFoo2.flatMap(s -> Option.of((String)null))
                                               .map(t -> t.toUpperCase() + "bar");

        System.out.println(maybeFooBar2.isEmpty());

        System.out.println("\n--- Try /tratar : ");

        System.out.println(Try.of(() -> replaceC.apply((String)null)).getOrElse("vino nula la vuelta"));

        Integer num1 = 2;
        Integer num2 = 1;

        ExecutorService executor = Executors.newFixedThreadPool(5);

        Either<?, ?> eitherValue = Try.of(() -> this.sum(num1, num2))
                .onFailure(throwable -> System.out.println("\nError : " + throwable.getMessage()))
                .onSuccess(integer -> System.out.printf("\nCorrecto se ha obtenido el numero = %s", integer))
                .andFinallyTry(() -> {
                    this.sendLogTemporary("Mensaje runnable");
                })
                .fold(throwable -> {
                            if (throwable.getMessage() != null) {
                                Future.run(executor,()-> this.sendLogTemporary(throwable.getMessage()));
                                return Either.right("Lef = "+throwable.getMessage());
                            } else {
                                return Either.left("Error in throwable");
                            }
                        },
                        integer -> {
                            if (integer != null) {
                                return Either.right("Right = "+integer);
                            } else {
                                return Either.left("Error no es un entero");
                            }
                        }
                );

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Finalizo temporizado");
        System.out.println(eitherValue.isRight() ? eitherValue.get() : eitherValue.swap().get());



        System.out.println("\n--- Lazy /Perezoso : ");
        Lazy<Double> lazy = Lazy.of(Math::random);
        lazy.isEvaluated(); // = false
        System.out.println(lazy.get());         // = 0.123 (random generated)
        lazy.isEvaluated(); // = true
        System.out.println(lazy.get());         // = 0.123 (memoized)

        CharSequence chars = Lazy.val(() -> "Yay!", CharSequence.class);
        System.out.println(chars);
        System.out.println("\n--- Either /Ya sea : ");
        Either<List<String>,List<Integer>>  value = computeListType(1);
        if(value.isLeft()){
            System.out.println("--- Con Swap");
            value.swap().get().forEach(System.out::println);
            System.out.println("--- Obteniendo el valor del left directamente");
            value.left().getOrNull().forEach(System.out::println);
        }else{
            value.get().forEach(System.out::println);
        }

       /* value.right().get().filter(x -> x != 1).forEach(System.out::println);
        value.swap().get().filter(x -> x.contains("o")).forEach(System.out::println);*/

        PersonValidator personValidator = new PersonValidator();

// Valid(Person(John Doe, 30))
        Validation<Seq<String>, Person> valid = personValidator.validatePerson("John Doe", 30);


// Invalid(List(Name contains invalid characters: '!4?', Age must be greater than 0))
        Validation<Seq<String>, Person> invalid = personValidator.validatePerson("John? Doe!4", -1);


    }

    private void sendLogTemporary(String message) throws InterruptedException {
        Thread.sleep(5000);
        System.out.println(Thread.currentThread().getName()+" - Llego mensaje despues de 5 seg = "+message);
    }

    public String methodWhichAccepts3Parameters (String s1,String s2,String s3) {
        return  replaceC.apply(s1)+ " - "+ replaceC.apply(s2)+ " - "+ replaceC.apply(s3);
    };

    private Function<String,String> replaceC = (inputString) -> inputString.toLowerCase().replace("c","");

    int sum(int first, int second) {
        if (first < 0 || second < 0) {
            throw new IllegalArgumentException("Only positive integers are allowed");
        }
        return first + second;
    }

    private Either<List<String>,List<Integer>> computeListType(Integer decision){
        if(decision == 1){
            return Either.left(List.of("uno","dos","tres"));
        }else{
            return Either.right(List.of(1,2,3));
        }

    }


}
