package com.test.dtos;

import lombok.experimental.SuperBuilder;

@SuperBuilder
public class Person {

    public final String name;
    public final int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person(" + name + ", " + age + ")";
    }

}
