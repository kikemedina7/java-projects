package com.test.dtos;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@AllArgsConstructor
@Data
@ToString

public class Customer  {

        private String name;
        private String lastName;
        private Integer identityNumber;
        private List<Product> products;

}
