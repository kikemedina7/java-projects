package com.test.dtos;


import lombok.Data;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.util.Objects;

@SuperBuilder
@Data
@ToString
public class Product implements Comparable{


    private String name;
    private String type;
    private BigDecimal price;

    @Override
    public int compareTo(Object o) {
        return name.compareTo(((Product)o).name);
    }

    @Override
    public String toString() {
        return "Product{" +
                ", hash='" + hashCode() + '\'' +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(name, product.name) &&
                Objects.equals(type, product.type) &&
                Objects.equals(price, product.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, price);
    }
}
