package com.alianzabe.constant;

public interface ResourceMapping {
    String USER = "/user";
    String EXPORT = "/export";
    String USER_BY_USERNAME = "/userbyUsername";
}
