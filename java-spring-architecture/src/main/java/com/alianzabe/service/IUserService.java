package com.alianzabe.service;


import com.alianzabe.dto.UserDto;
import com.alianzabe.exception.NoContentException;

import java.util.List;


public interface IUserService {

    List<UserDto> getUsers() throws NoContentException;

    UserDto getUser(Long id) throws NoContentException;

    UserDto createUser(UserDto user);

    UserDto updateUser(UserDto user);

    void deleteUser(UserDto user);

    UserDto findUserByUsername(String username) throws NoContentException;

}
