package com.alianzabe.service.impl;

import com.alianzabe.domain.User;
import com.alianzabe.dto.UserDto;
import com.alianzabe.exception.NoContentException;
import com.alianzabe.mapper.IUserMapper;
import com.alianzabe.repository.IUserRepository;
import com.alianzabe.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class UserService implements IUserService {

    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IUserMapper userMapper;

    @Override
    public List<UserDto> getUsers() throws NoContentException {
        return userMapper.entityToDto(userRepository.findAll());
    }

    @Override
    public UserDto getUser(Long id) throws NoContentException {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return userMapper.entityToDto(user.get());
        }
        throw new NoContentException();
    }

    @Override
    public UserDto createUser(UserDto user) {
        User userEntity = userMapper.dtoToEntity(user);
        return userMapper.entityToDto(userRepository.save(userEntity));
    }

    @Override
    public UserDto updateUser(UserDto user) {
        User userEntity = userMapper.dtoToEntity(user);
        return userMapper.entityToDto(userRepository.save(userEntity));
    }

    @Override
    public void deleteUser(UserDto user) {
        userRepository.delete(userMapper.dtoToEntity(user));
    }

    @Override
    public UserDto findUserByUsername(String username) throws NoContentException{
        return  userMapper.entityToDto(userRepository.findUser(username));
    }
}
