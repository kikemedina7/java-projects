import java.util.List;
import java.util.function.Predicate;

public class Persona {

    public static final int OLD_PEOPLE = 40;
    private String nombre;
    private String apellidos;
    private int edad;





    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public int getEdad() {
        return edad;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Persona(String nombre, String apellidos, int edad) {
        super();
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
    }

    public static Predicate<Persona> filterOldPeople(){
        Predicate<Persona> pr1 = p -> p.getEdad() <= 30;
        return pr1;
    }

    public static double sumWithCondition(List<Persona> listaPersonas, Predicate<Persona> predicatePersona){
        //Foreach
        return listaPersonas.stream().filter(predicatePersona).mapToDouble(per -> per.getEdad()).sum();
    }

    @Override
    public String toString() {
        return "Persona ["+this.nombre+","+this.apellidos+","+this.edad+" ]";
    }

    public  String printNombreAfterTimeOut(){
        System.out.println(getNombre());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return String.format(" Hola %s %s eres bienvenido a mi programa",this.nombre,this.apellidos);
    }
}