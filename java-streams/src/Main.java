import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.function.IntConsumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {


    public static void main(String[] args) throws IOException {

        /*
        *  Java collections con programacion funcional
        *  4 tipos de lambda
        *  - Consumidores : no devuelven resultados
        *  - Proveedores/Supplier : no tienen parametros pero devuelven resultados
        *  - Funciones : aceptan argumentos (parametros), devuelven valores como resultados y los tipos de datos no tienen que ser iguales
        *  - Referencia a Metodos
        *       1) Metodos estaticos
        *       2) Metodos de instancia de un tipo
        *       3) Metodos de instancias de un objeto existente
        *
        *
        * */

        /*
            - Referencia a Metodos
        *       1) Metodos estaticos
        */


        /*   ########### JAVA 8 USO DE STREAMS ############### */

        //Iniciando lista
        List<Optional<String>> lista = Arrays.asList(
                Optional.of("hola"),
                Optional.empty(),
                Optional.of("que"),
                Optional.of("tal"));

        lista.stream()
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(System.out::println);

        // ]Java Predicate Interface y sus metodos
        System.out.println(" \n --------------- Java predicate :");

        List<Persona> listaPersonas= new ArrayList<>();

        Persona p1= new Persona ("pepe","perez",20);
        Persona p2= new Persona ("ana","perez",30);
        Persona p3= new Persona ("gema","sanchez",40);
        Persona p4= new Persona ("pedro","gomez",70);

        listaPersonas.add(p1);
        listaPersonas.add(p2);
        listaPersonas.add(p3);
        listaPersonas.add(p4);

        Predicate<Persona> predicado1= p->p.getEdad()>18;
        Predicate<Persona> predicado2= p->p.getEdad()<30;
        Predicate<Persona> predicado3= predicado1.and(predicado2);
        Predicate<Persona> predicado4= p->p.getEdad()>65;
        Predicate<Persona> predicado5=predicado4.or(predicado3);

        listaPersonas.stream().filter(predicado1).forEach( item -> System.out.println(item.toString()));


        //Uso
        System.out.println(" \n --------------- Sorted Persona :");
        List<Persona> listaPersona2 = Arrays.asList(p1, p4, p3,p2);
        System.out.println(" \n Antes :");
        listaPersona2.forEach(item -> System.out.println(item.toString()));

        //Mejora


        //Codigo alterno
        listaPersona2.sort((per1, per2)-> {
            return per1.getEdad() <= per2.getEdad() ? 1 : -1;
        });
        System.out.println(" \n Despues :");

        listaPersona2.forEach(item -> System.out.println(item.toString()));

        //Uso de pattern Java Pattern Predicate y filtrados
        System.out.println(" \n --------------- Java Pattern :");
        Pattern patron=Pattern
                .compile("\\d+");

        Stream<String> mistream=Stream.of("hola","4","6","7","estas","9");

        mistream.filter(patron.asPredicate())
                .map(Integer::parseInt)
                .filter(n->n>5)
                .forEach(System.out::println);

        //Uso de pattern Java Pattern Predicate y filtrados
        System.out.println(" \n --------------- Java Stream Reduce:");
        int resultado= IntStream.of(5,2,7,9).reduce(0,Integer::sum);
        System.out.println(resultado);
        System.out.println(" \n --- Second example : Buscando la palabra que tenga la maxima cantidad de caracteres");
        // creating a list of Strings
        List<String> words = Arrays.asList("GFG", "Geeks", "for",
                "GeeksQuiz", "GeeksforGeeks");

        // The lambda expression passed to
        // reduce() method takes two Strings
        // and returns the longer String.
        // The result of the reduce() method is
        // an Optional because the list on which
        // reduce() is called may be empty.
        Optional<String> longestString = words.stream()
                .reduce((word1, word2)
                        -> word1.length() > word2.length()
                        ? word1 : word2);

        // Displaying the longest String
        longestString.ifPresent(System.out::println);
        System.out.println(" \n --- Third example : Sumando edades de todos");

        int edad = listaPersona2.stream()
                .filter(Persona.filterOldPeople())
                .reduce(0,
                        (partialAgeResul,pe2) -> partialAgeResul + pe2.getEdad(),
                        Integer::sum
                );
        System.out.println(edad);
        System.out.println(" \n --- Fourth example : Sumando edades de todos");
        List<Persona> listaPersona3 = Arrays.asList(p1, p4, p3,p2);
        List<Persona> listaPersona4 = Arrays.asList(p1, p4, p3,p2);

        System.out.println(" \n --- five example : Iterando los valores solo mapeando un valor dentro del objeto");
        listaPersona3.stream().filter(t -> t.getEdad() >= 40).mapToDouble(per -> per.getEdad()).forEach(System.out::println);
        System.out.println(" \n --- six example : obteniendo solo lista de nombres a partir de una lista de objectos");
        List<String> lNombres = listaPersona4.stream().filter(t -> t.getEdad() >= 40).map(Persona::toString).collect(Collectors.toList());
        lNombres.forEach(System.out::println);
        System.out.println(" \n --- seven : Mapeando una funcion con previa condicion el el predicado");
        System.out.println(Persona.sumWithCondition(listaPersona3, t -> t.getEdad() >= 40));

        //Parallell Stream parallelStream ¿Y cómo se hace para tener código paralelo? En Java SE 8 es fácil: solo es necesario reemplazar la instrucción stream() por parallelStream(), como se muestra en la Secuencia 3, y la API de streams descompondrá internamente la consulta para aprovechar los núcleos múltiples de la computadora.

        System.out.println(" \n --------------------------------------------- Parallell Stream ");
        List<String> lNombres2 = listaPersona4.parallelStream().filter(t -> t.getEdad() >= 40).map(Persona::toString).collect(Collectors.toList());
        lNombres2.forEach(System.out::println);

        //Streams complejos

        System.out.println(" \n --------------------------------------------- Programacion funcional = con retornos despues de operaciones");
        System.out.println(" \n ---- 1) Usando Limit");
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> twoEvenSquares =
                numbers.stream()
                        .filter(n -> {
                            System.out.println("filtering " + n);
                            return n % 2 == 0;
                        })
                        .map(n -> {
                            System.out.println("mapping " + n);
                            return n * n;
                        })
                        .limit(3) // Cuando se tenga el segundo resultado mapeado se corta el flujo del stream
                        .collect(Collectors.toList());
        System.out.println(twoEvenSquares);
        System.out.println(" \n ---- 1) Usando Skip");
         twoEvenSquares =
                numbers.stream()
                        .filter(n -> {
                            System.out.println("filtering " + n);
                            return n % 2 == 0;
                        })
                        .map(n -> {
                            System.out.println("mapping " + n);
                            return n * n;
                        })
                        .skip(3) // luego de acertar 3 items pasando del filter al map, es que los empieza a mapear
                        .collect(Collectors.toList());
        System.out.println(twoEvenSquares);

        System.out.println(" \n --------------------------------------------- Programacion funcional  anyMatch, allMatch y noneMatch");
        boolean expensive =
                numbers.stream()
                        .anyMatch(t -> t == 2);
        System.out.println(expensive);
         expensive =
                numbers.stream()
                        .allMatch(t -> t != 10);
        System.out.println(expensive);
         expensive =
                numbers.stream()
                        .noneMatch(t -> t == 13);
        System.out.println(expensive);
        System.out.println(" \n --------------------------------------------- Programacion funcional  findFirst y findAny");

        Optional<Persona> findAny=
        listaPersona4.stream()
                .filter(t -> t.getEdad() >=  Persona.OLD_PEOPLE)
                .findAny();
        if(findAny.isPresent()){
            System.out.println(findAny.get().toString());
        }

        findAny=
        listaPersona4.stream()
                .filter(t -> t.getEdad() >=  Persona.OLD_PEOPLE)
                .findFirst();
        if(findAny.isPresent()){
            System.out.println(findAny.get().toString());
        }

        System.out.println(" \n --------------------------------------------- Programacion funcional  Map");

        List<String> wordss = Arrays.asList("Oracle", "Java", "Magazine");
        List<Integer> wordLengths =
                wordss.stream()
                        .map(String::length)
                        .collect(Collectors.toList());
        wordLengths.forEach(System.out::println);

        System.out.println(" \n --------------------------------------------- Programacion funcional  Reduce");
        int product = numbers.stream().reduce(1, (a, b) -> a * b);
        System.out.println(product);
        product = numbers.stream().reduce(1, Integer::max);
        System.out.println(product);
        product = numbers.stream().reduce(1, Integer::min);
        System.out.println(product);
        product = numbers.stream().reduce(1, Integer::sum); //suma No recomendado
        System.out.println(product);

        System.out.println(" \n --------------------------------------------- Programacion funcional  Sum"); //Suma recomendada
        int statementSum =
                listaPersona4.stream()
                        .mapToInt(Persona::getEdad)
                        .sum(); // works!
        System.out.println(statementSum);

        System.out.println(" \n --------------------------------------------- Programacion funcional  IntStream y ranged/rangeClosed"); //Suma recomendada
        System.out.println("\n --- RangeClosed ");
        IntStream oddNumbers =
                IntStream.rangeClosed(10, 30)
                        .filter(n -> n % 2 == 0);
        oddNumbers.forEach(System.out::println);
        System.out.println("\n --- Range ");
         oddNumbers =
                IntStream.range(10, 30)
                        .filter(n -> n % 2 == 0);
        oddNumbers.forEach(System.out::println);

        System.out.println(" \n --------------------------------------------- Programacion funcional  Creacion de Streams"); //Suma recomendada
        System.out.println("\n --- From Stream Class ");
        Stream<Integer> numbersFromValues = Stream.of(1, 2, 3, 4);
        numbersFromValues.forEach(System.out::println);

        System.out.println("\n --- From Arrays ");
        int[] numb = {1, 2, 3, 4};
        IntStream numbersFromArray = Arrays.stream(numb);
        numbersFromArray.forEach(System.out::println);

        System.out.println("\n --- From File text ");
        long numberOfLines =
                Files.lines(Paths.get("src/file.txt"), Charset.defaultCharset())
                        .count();
        String lineAgregar = "\nEsta es la linea "+(numberOfLines+1);

        Files.write(Paths.get("src/file.txt"),lineAgregar.getBytes(), StandardOpenOption.APPEND);
        System.out.println(numberOfLines);


        System.out.println(" \n --------------------------------------------- Programacion funcional  Uso avanzado predicados"); //Suma recomendada

        Libro l = new Libro("El señor de los anillos", "fantasia", 1100);
        Libro l2 = new Libro("El Juego de Ender", "ciencia ficcion", 500);
        Libro l3 = new Libro("La fundacion", "ciencia ficcion", 500);
        Libro l4 = new Libro("Los pilares de la tierra", "historica", 1200);
        List <Libro> listaLibros = Arrays.asList(l, l2, l3, l4);
        listaLibros
                .stream()
                .filter(LibroUtils::buenosLibros)
                .map(Libro::getNombre)
                .forEach(System.out::println);

        System.out.println("\n------------------------------- Programacion Funcional : Usando Collecciones / Mapas");
        Map<String, String> books = new HashMap<>();
        books.put(
                "978-0201633610", "Design patterns : elements of reusable object-oriented software");
        books.put(
                "978-1617291999", "Java 8 in Action: Lambdas, Streams, and functional-style programming");
        books.put("978-0134685991", "Effective Java");

        books.entrySet().stream()
                .filter(e -> "Effective Java".trim().toLowerCase().equals(e.getValue().trim().toLowerCase()))
                .map(Map.Entry::getKey)
                .findFirst();


        Libro.printBookList(new Libro("jorge","1",10),new Libro("jorge","1",10));

        System.out.println("\n------------------------------- Programacion Funcional : ParallelStream para ejecucion de streams por hilos(threads) \n Primero se ejecutan las funciones en MAP y despues ejecuta el for each, se ehecuta cada iteracion con cada hilo independiente y luego de que terminan todos prosigue.");
        long start = System.currentTimeMillis();
        listaPersona4.parallelStream().map(Persona::printNombreAfterTimeOut).forEach(System.out::println);
        long end = System.currentTimeMillis();
        System.out.println(String.format("Concurrent time (%s ms)...", (end - start)));

        System.out.println("\n------------------------------- Programacion Funcional : stream iterate ");
        Stream.iterate(1, valor -> valor * 2).limit(4).forEach(item -> System.out.println("\n"+item));

    }

}

