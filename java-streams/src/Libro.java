import java.util.Arrays;

public class Libro {

    private String nombre;
    private String categoria;
    private Integer paginas;

    public Libro(String nombre, String categoria, Integer paginas) {
        this.nombre = nombre;
        this.categoria = categoria;
        this.paginas = paginas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Integer getPaginas() {
        return paginas;
    }

    public void setPaginas(Integer paginas) {
        this.paginas = paginas;
    }

    public static void printBookList(Libro... listaLibros){
        Arrays.asList(listaLibros).stream().map(Libro::getNombre).forEach(System.out::println);
    }
}
