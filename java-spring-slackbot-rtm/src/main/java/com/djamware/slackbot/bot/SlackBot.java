/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djamware.slackbot.bot;


import me.ramswaroop.jbot.core.slack.Bot;
import me.ramswaroop.jbot.core.slack.Controller;
import me.ramswaroop.jbot.core.slack.EventType;
import me.ramswaroop.jbot.core.slack.models.Event;
import me.ramswaroop.jbot.core.slack.models.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

/**
 *
 * @author didin
 */
@Component
public class SlackBot extends Bot {


    private static final Logger logger = LoggerFactory.getLogger(SlackBot.class);

    @Value("${slackBotToken}")
    private String slackToken;

    @Override
    public String getSlackToken() {
        return slackToken;
    }

    @Override
    public Bot getSlackBot() {
        return this;
    }
    
    @Controller(events = {EventType.DIRECT_MENTION, EventType.DIRECT_MESSAGE}, pattern = "hi|Hola|Hola")
    public void onReceiveDM(WebSocketSession session, Event event) {
        String msj = "";
            msj = "Hola, Soy " + slackService.getCurrentUser().getName() +
                    "\nQue desea consultar : :sonrisa_burlona:"+
                    "\n(1) Todos los servicios :sonrojado:"+
                    "\n(2) DataPower : CustomerQuery ";
        reply(session, event, new Message(msj));
    }

    @Controller(events = {EventType.DIRECT_MENTION, EventType.DIRECT_MESSAGE}, pattern = "1|2|3|4")
    public void onReceiveDMConsumerService(WebSocketSession session, Event event) {
        String msj = "";
        switch (Integer.valueOf(event.getText()).intValue()) {
            case 1:
                msj = "los servicios son : customerQuey, advisorQuery,etc..";
            default:
                break;
        }
        reply(session, event, new Message(msj));
    }

}
