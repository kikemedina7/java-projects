package com.spring.rabbitmq.demo.mq.config;

import com.spring.rabbitmq.demo.mq.tutorial.MessageReceiver;
import com.spring.rabbitmq.demo.mq.tutorial.MessageSender;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile ({"tut1", "hello-world"})
@Configuration
public class RabbitmqConfig {

    @Bean
    public Queue hello() {
        return new Queue("hello");
    }

    @Profile("receiver")
    @Bean
    public MessageReceiver receiver() {
        return new MessageReceiver();
    }

    @Profile("sender")
    @Bean
    public MessageSender sender() {
        return new MessageSender();
    }
}
