package com.edwise.pocs.completablefuturepoc;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;

public class CompletableFutureAdvanceTest {
    private final static Logger LOGGER = LoggerFactory.getLogger(CompletableFutureAdvanceTest.class);

    private ExecutorService executor;

    @Before
    public void setUp() {
        executor = Executors.newFixedThreadPool(5);
    }

    @After
    public void shutDown() throws InterruptedException {
        Sleep.sleepSeconds(5);
        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.SECONDS);
    }

    @Test
    public void testServiceExecutor(){
        Runnable runnableTask = () -> {
            try {
                TimeUnit.MILLISECONDS.sleep(300);
                LOGGER.info("Task's execution");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        Callable<String> callableTask = () -> {
            TimeUnit.MILLISECONDS.sleep(300);
            LOGGER.info("Task's execution [{}]", Thread.currentThread().getThreadGroup().getParent().getName());
            return "Task's execution";
        };

        List<Callable<String>> callableTasks = new ArrayList<>();
        callableTasks.add(callableTask);
        callableTasks.add(callableTask);
        callableTasks.add(callableTask);

        //Usando execute() se ejecuta por hilo cuando se manda un runnable

//        executor.execute(runnableTask);

        // Usando submit () envía una tarea ejecutable o ejecutable a un ExecutorService y devuelve un resultado de tipo Future .
        Future<String> future =
                executor.submit(callableTask);
        if(future.isDone()){
            try {
                LOGGER.info("submit : {}",future.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        // Usando invokeAny () asigna una colección de tareas a un ExecutorService, haciendo que cada una se ejecute y devuelve el resultado de una ejecución exitosa de una tarea (si hubo una ejecución exitosa)

        try {
            String result = executor.invokeAny(callableTasks);
            LOGGER.info("Invoke any : {}",result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        // Usando nvokeAll () asigna una colección de tareas a un ExecutorService, haciendo que cada una se ejecute y devuelve el resultado de todas las ejecuciones de tareas en forma de una lista de objetos de tipo Future .

        try {
            List<Future<String>> allTask = executor.invokeAll(callableTasks);
            for(Future<String> task : allTask){
                if(task.isDone())
                    LOGGER.info("Invoke All succes : {}",task.get());
                else
                    LOGGER.info("Invoke All cancelled : {}",task.get());

            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

//        Forma correcta de terminar la ejecucion de hilos
        executor.shutdown();
        try {
            if (!executor.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                executor.shutdownNow();
            }
        } catch (InterruptedException e) {
            executor.shutdownNow();
        }


    }

    @Test
    public void testFutureThenApply() {
        CompletableFuture<String> futureAsync = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando supplyAsync for thenApply...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado supplyAsync for thenApply!");
            return "Terminado";
        }, executor);

        CompletableFuture<String> futureApply = futureAsync.thenApplyAsync(s -> {
            LOGGER.info("Comenzando applyAsync...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado applyAsync!");
            return s.toUpperCase();
        }, executor);

        futureApply.whenCompleteAsync((s, e) -> LOGGER.info("Resultado applyAsync: {}", s), executor);
        LOGGER.info("Terminado main thread");
    }



    @Test
    public void testThreadPoolExecutor() {
        ThreadPoolExecutor executor =
                (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
        executor.submit(() -> {
            Thread.sleep(10000);
            return null;
        });
        executor.submit(() -> {
            Thread.sleep(10000);
            return null;
        });
        executor.submit(() -> {
            Thread.sleep(10000);
            return null;
        });
        LOGGER.info("Pool Size = {}",executor.getPoolSize());
        LOGGER.info("Queue = {}",executor.getQueue().size());
        assertEquals(2, executor.getPoolSize());
        assertEquals(1, executor.getQueue().size());
    }

    @Test
    public void testCacheThreadPoolExecutor() {
        ThreadPoolExecutor executor =
                (ThreadPoolExecutor) Executors.newCachedThreadPool();
        executor.submit(() -> {
            Thread.sleep(1000);
            return null;
        });
        executor.submit(() -> {
            Thread.sleep(1000);
            return null;
        });
        executor.submit(() -> {
            Thread.sleep(1000);
            return null;
        });

        LOGGER.info("Pool Size = {}",executor.getPoolSize());
        LOGGER.info("Queue = {}",executor.getQueue().size());
        assertEquals(3, executor.getPoolSize());
        assertEquals(0, executor.getQueue().size());

    }

    @Test
    public void testNewSingleThreadExecutor() {
        AtomicInteger counter = new AtomicInteger();

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            Sleep.sleepSeconds(3);
            counter.set(1);
            LOGGER.info("counter = {}",counter);
        });
        executor.submit(() -> {
           LOGGER.info(" compare = {}",counter.compareAndSet(1, 2));
        });

    }

    @Test
    public void testScheduledExecutorService() throws InterruptedException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(5);
        executor.schedule(() -> {
            System.out.println("Hello World");
        }, 500, TimeUnit.MILLISECONDS);

        CountDownLatch lock = new CountDownLatch(3);

        ScheduledExecutorService executor2 = Executors.newScheduledThreadPool(5);
        ScheduledFuture<?> future = executor2.scheduleAtFixedRate(() -> {
            System.out.println("Hello World");
            lock.countDown();
        }, 500, 1000, TimeUnit.MILLISECONDS);

        lock.await(5000, TimeUnit.MILLISECONDS);
        future.cancel(true);
    }

    @Test
    public void testFutureThenAccept() {
        CompletableFuture<String> futureAsync = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando supplyAsync for thenAccept...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado supplyAsync for thenAccept!");
            return "Terminado";
        }, executor);

        futureAsync.thenAcceptAsync(s -> {
            LOGGER.info("Comenzando thenAccept...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado thenAccept!");
            LOGGER.info("Resultado: {}", s);
        }, executor);
        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureThenRun() {
        CompletableFuture<Void> futureRun = CompletableFuture.runAsync(() -> {
            LOGGER.info("Comenzando runAsync for thenRun...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado runAsync for thenRun!");
        }, executor);

        futureRun.thenRunAsync(() -> {
            LOGGER.info("Comenzando thenRun...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado thenRun!");
        }, executor);
        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureWithExceptionWhenComplete() {
        CompletableFuture<String> futureAsync = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando supplyAsync with exception...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado supplyAsync with exception!");
            throw new RuntimeException("Error en el futuro");
        }, executor);

        futureAsync.whenCompleteAsync((s, e) -> {
            if (e != null) {
                LOGGER.error("Resultado con excepción!!", e);
            } else {
                LOGGER.info("Resultado applyAsync: {}", s);
            }
        }, executor);
        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureWithExceptionExceptionally() {
        LOGGER.info("exceptionally : Permite crear un hilo donde solo se maneje la excepcion pero no la respuesta del hilo anterior si es correcta");
        CompletableFuture<String> futureAsync = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando supplyAsync with exception...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado supplyAsync with exception!");
            throw new RuntimeException("Error en el futuro");
        }, executor);

        CompletableFuture<String> futureEx = futureAsync.exceptionally(e -> {
            LOGGER.error("Resultado con excepción!!", e);
            return "StringPorDefecto";
        });

        futureEx.whenCompleteAsync((s, e) -> LOGGER.info("Resultado futureEx: {}", s), executor);
        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureWithExceptionHandle() {
        LOGGER.info("handleAsync : Handler de hilos que permite manejar una excepcion en una ejecucion de hilo," +
                "\n y de no lanzar la excepcion maneja la respuesta del hilo anterior");
        CompletableFuture<String> futureAsync = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando supplyAsync with exception...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado supplyAsync with exception!");
            throw new RuntimeException("Error en el futuro");
        }, executor);

        CompletableFuture<String> handledFuture = futureAsync.handleAsync((s, e) -> {
            if (e != null) {
                LOGGER.error("Resultado con excepción!!", e);
                return "StringPorDefecto";
            } else {
                LOGGER.info("Resultado: {}", s);
                return s;
            }
        }, executor);

        handledFuture.whenCompleteAsync((s, e) -> LOGGER.info("Resultado handle: {}", s), executor);
        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureThenCompose() {
        LOGGER.info("thenComposeAsync : genera hilos secuenciales donde se espera que termine el primero para luego orquestar el siguiente. DE FORMA SECUENCIAL");
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando supplyAsync for thenCompose...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado supplyAsync for thenCompose!");
            return "Terminado";
        }, executor);

        CompletableFuture<String> fCompose =
                future1.thenComposeAsync(s -> CompletableFuture.supplyAsync(() -> {
                            LOGGER.info("Comenzando thenCompose...");
                            Sleep.sleepSeconds(2);
                            LOGGER.info("Terminado thenCompose!");
                            return s.concat(" + Terminado other");
                        }, executor),
                        executor);

        fCompose.whenCompleteAsync((s, e) -> LOGGER.info("Resultado thenCompose: {}", s), executor);
        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureThenCombine() {
        LOGGER.info("thenCombineAsync : Se ejecuta cuando otros dos hilos se completan, implementa un BiFunction que tiene las dos respuestas de los hilos como argumentos de entrada" +
                "\n Y tiene salida del objeto");
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future1 for thenCombine...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado future1 for thenCombine!");
            return "Terminado";
        }, executor);

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future2 for thenCombine...");
            Sleep.sleepSeconds(1);
            LOGGER.info("Terminado future2 for thenCombine!");
            return "Terminado other";
        }, executor);

        CompletableFuture<StringBuilder> fCombine = future1.thenCombineAsync(future2, (s1, s2) -> {
            LOGGER.info("En el thenCombine, recibidos results: {}, {}", s1, s2);
            return new StringBuilder().append(s1).append(" , ").append(s2);
        }, executor);

        fCombine.whenCompleteAsync((s, e) -> LOGGER.info("Resultado thenCombine: {}", s.toString()), executor);
        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureThenAcceptBoth() {
        LOGGER.info("thenAcceptBothAsync : Se ejecuta cuando otros dos hilos se completan, implementa un BiConsumer que tiene las dos respuestas de los hilos como argumentos de entrada");
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future1 for thenAcceptBoth...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado future1 for thenAcceptBoth!");
            return "Terminado";
        }, executor);

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future2 for thenAcceptBoth...");
            Sleep.sleepSeconds(1);
            LOGGER.info("Terminado future2 for thenAcceptBoth!");
            return "Terminado other";
        }, executor);

        future1.thenAcceptBothAsync(future2, (s1, s2) ->
                        LOGGER.info("En el thenAcceptBoth, recibidos results: {}, {}", s1, s2)
                , executor);

        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureRunAfterBoth() {
        LOGGER.info("runAfterBothAsync : Se ejecuta cuando otros dos hilos se completan, implemenbta Runnable por lo que no se pueden obtener resultados de los hilos");
        CompletableFuture<Void> future1 = CompletableFuture.runAsync(() -> {
            LOGGER.info("Comenzando future1 for runAfterBoth...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado future1 for runAfterBoth!");
        }, executor);

        CompletableFuture<Void> future2 = CompletableFuture.runAsync(() -> {
            LOGGER.info("Comenzando future2 for runAfterBoth...");
            Sleep.sleepSeconds(1);
            LOGGER.info("Terminado future2 for runAfterBoth!");
        }, executor);

        future1.runAfterBothAsync(future2, () -> LOGGER.info("En el runAfterBoth, futuros terminados.")
                , executor);

        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureAcceptEither() {
        LOGGER.info("acceptEitherAsync : solo acepta el primer hilo que dure menos tiempo en ejecucion");
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future1 for acceptEither...");
            Sleep.sleepSeconds(3);
            LOGGER.info("Terminado future1 for acceptEither!");
            return "Segundo";
        }, executor);

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future2 for acceptEither...");
            Sleep.sleepSeconds(1);
            LOGGER.info("Terminado future2 for acceptEither!");
            return "Primero";
        }, executor);

        future1.acceptEitherAsync(future2, (s) ->
                        LOGGER.info("En el acceptEither, recibido el primer resultado: {}", s)
                , executor);

        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureRunAfterEither() {
        LOGGER.info("runAfterEitherAsync : Lo mismo que acceptEitherAsync, pero no acepta respuestas de los hilo. Implementa void Runnable");
        CompletableFuture<Void> future1 = CompletableFuture.runAsync(() -> {
            LOGGER.info("Comenzando future1 for runAfterEither...");
            Sleep.sleepSeconds(3);
            LOGGER.info("Terminado future1 for runAfterEither!");
        }, executor);

        CompletableFuture<Void> future2 = CompletableFuture.runAsync(() -> {
            LOGGER.info("Comenzando future2 for runAfterEither...");
            Sleep.sleepSeconds(1);
            LOGGER.info("Terminado future2 for runAfterEither!");
        }, executor);

        future1.runAfterEitherAsync(future2, () -> LOGGER.info("En el runAfterEither, primero terminado.")
                , executor);

        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureApplyToEither() {
        LOGGER.info("applyToEitherAsync : Lo mismo que acceptEitherAsync, devuelve un CompletableFuture. " +
                "\ningresa el primero hilo que termine y devuelve un objeto CompletableFuture<T>");
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future1 for applyToEither...");
            Sleep.sleepSeconds(3);
            LOGGER.info("Terminado future1 for applyToEither!");
            return "Segundo";
        }, executor);

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future2 for applyToEither...");
            Sleep.sleepSeconds(1);
            LOGGER.info("Terminado future2 for applyToEither!");
            return "Primero";
        }, executor);

        CompletableFuture<String> applyToEitherFuture = future1.applyToEitherAsync(future2, s -> {
            LOGGER.info("Comenzando applyToEither...");
            Sleep.sleepSeconds(1);
            LOGGER.info("Terminado applyToEither!");
            return s.toUpperCase();
        }, executor);

        applyToEitherFuture.whenCompleteAsync((s, e) -> LOGGER.info("Resultado applyToEither: {}", s),
                executor);
        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureAllOf() throws ExecutionException, InterruptedException {
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future1 for allOf...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado future1 for allOf!");
            return "Terminado future1";
        }, executor);

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future2 for allOf...");
            Sleep.sleepSeconds(1);
            LOGGER.info("Terminado future2 for allOf!");
            return "Terminado future2";
        }, executor);

        CompletableFuture<String> future3 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future3 for allOf...");
            Sleep.sleepSeconds(3);
            LOGGER.info("Terminado future3 for allOf!");
            return "Terminado future3";
        }, executor);

        CompletableFuture<Void> all = CompletableFuture.allOf(future1, future2, future3);

        all.whenCompleteAsync((s, e) -> LOGGER.info("Resultado all: {}", s), executor);
        LOGGER.info("Terminado main thread");
    }

    @Test
    public void testFutureAnyOf() throws InterruptedException {
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future1 for allOf...");
            Sleep.sleepSeconds(2);
            LOGGER.info("Terminado future1 for allOf!");
            return "Terminado future1";
        }, executor);

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future2 for allOf...");
            Sleep.sleepSeconds(1);
            LOGGER.info("Terminado future2 for allOf!");
            return "Terminado future2";
        }, executor);

        CompletableFuture<String> future3 = CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Comenzando future3 for allOf...");
            Sleep.sleepSeconds(7);
            LOGGER.info("Terminado future3 for allOf!");
            return "Terminado future3";
        }, executor);

        CompletableFuture<Object> all = CompletableFuture.anyOf(future1, future2, future3);

        all.whenCompleteAsync((s, e) -> LOGGER.info("Resultado any: {}", s), executor);
        LOGGER.info("Terminado main thread");
    }
}
