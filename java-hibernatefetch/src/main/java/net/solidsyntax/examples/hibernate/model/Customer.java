package net.solidsyntax.examples.hibernate.model;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Customer {

    @Id
    private Integer id;
    
    @Column(length = 20)
    private String firstName;

    @Column(length = 30)
    private String lastName;

    @Column(length = 50)
    private String street;

    @Column(length = 25)
    private String city;
    
    @OneToMany(mappedBy = "customer")
//    @Fetch(FetchMode.SUBSELECT)
//    @Fetch(FetchMode.JOIN)
    @Fetch(FetchMode.SELECT)
//    @BatchSize(size = 25)
    private Set<Invoice> invoices;

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public Set<Invoice> getInvoices() {
        return invoices;
    }
    
}