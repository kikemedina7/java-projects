package net.solidsyntax.examples.hibernate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import net.solidsyntax.examples.hibernate.model.Customer;
import net.solidsyntax.examples.hibernate.model.Invoice;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateFetchModeExample {

    private SessionFactory sessionFactory;

    public static void main(String[] args) {
        HibernateFetchModeExample example = new HibernateFetchModeExample();
        example.run();
    }

    private void run() {
        configureHibernate();
        findTotalAmountForAllInvoice();
    }
    
    private void configureHibernate() {
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Customer.class);
        configuration.addAnnotatedClass(Invoice.class);
        configuration.configure();
        ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
        sessionFactory =  configuration.buildSessionFactory(serviceRegistry);
    }

    private void findTotalAmountForAllInvoice() throws HibernateException {
        Session session = sessionFactory.openSession();
     
        System.out.println("Load all Customers:");
        
        Criteria crit = session.createCriteria(Customer.class);
        @SuppressWarnings("unchecked")
        List<Customer> customers = crit.list();
      
        System.out.println("Number of Customers: " + customers.size());
        BigDecimal allCustomerTotal = BigDecimal.ZERO;
        for (Customer customer : customers) {
            System.out.println(String.format("Fetch the collection of Invoices for Customer %s %s", customer.getFirstName(), customer.getLastName()));
            Set<Invoice> invoices = customer.getInvoices();
            Integer count = 0;
            Integer totalInvoiceForCustomer = 0;
            for (Invoice invoice : invoices) {
                allCustomerTotal = allCustomerTotal.add(invoice.getTotal());
                totalInvoiceForCustomer = invoice.getTotal().intValue();
                System.out.println(String.format("Fetch the collection of Invoices for Customer %s %s - Acum invoice [%s] - Total For Iterate ", customer.getFirstName(), customer.getLastName(),allCustomerTotal,totalInvoiceForCustomer));
                count++;
            }
            System.out.println(String.format("Fetch the collection of Invoices for Customer %s %s - Total Invoices [%s]", customer.getFirstName(), customer.getLastName(),count));
        }
        System.out.println("Total amount for all invoices: "+ allCustomerTotal.toString());
        session.close();
    }
 
}
