package net.solidsyntax.examples.hibernate.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Invoice {

    @Id
    private Integer id;
    
    @Column(columnDefinition="decimal", precision=128 , scale=0) 
    private BigDecimal total;
    
    @ManyToOne
    @JoinColumn(name = "CUSTOMERID")
    private Customer customer;

    public Integer getId() {
        return id;
    }

    public BigDecimal getTotal() {
        return total;
    }
    
    public Customer getCustomer() {
        return customer;
    }  
}
